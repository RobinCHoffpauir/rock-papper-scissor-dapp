//SPDX-License-Identifier: license
pragma solidity ^0.8.11;

contract Game {
    // Move
    enum Moves {
        None,
        Rock,
        Paper,
        Scissor
    }
    enum Outcomes {
        None,
        PlayerA,
        PlayerB,
        Draw
    }

    // Players address
    address payable private playerA;
    address payable private playerB;

    // Players Choice
    bytes32 private encPlayerAChoice;
    bytes32 private encPlayerBChoice;

    address public check;

    Moves private movePlayerA;
    Moves private movePlayerB;

    // ===================Register Phase Start=======================
    modifier validateAlreadyRegistered() {
        require(
            msg.sender != playerA && msg.sender != playerB,
            "Already registered"
        );
        _;
    }

    function join() public validateAlreadyRegistered returns (uint8) {
        if (playerA == address(0x0)) {
            playerA = payable(msg.sender);
            return 1;
        } else if (playerB == address(0x0)) {
            playerB = payable(msg.sender);
            return 2;
        }
        return 0;
    }

    // ===================Register Phase End=======================
    // ===================Commit Phase Start=======================
    function play(bytes32 encrMove) public returns (bool) {
        if (msg.sender == playerA && encPlayerAChoice == 0x0) {
            encPlayerAChoice = encrMove;
        } else if (msg.sender == playerB && encPlayerBChoice == 0x0) {
            encPlayerBChoice = encrMove;
        } else {
            return false;
        }
        return true;
    }

    // ===================Commit Phase End=======================
    // Real 
    function reveal(string memory clearMove) public returns (Moves) {
        bytes32 encrMove = keccak256(abi.encodePacked(clearMove));
        // Moves move =
        Moves move = Moves(getFirstChar(clearMove)); // Actual move (Rock / Paper / Scissor)

        // If move invalid, exit
        if (move == Moves.None) {
            return Moves.None;
        }

        // If hashes match, clear move is saved
        if (msg.sender == playerA && encrMove == encPlayerAChoice) {
            movePlayerA = move;
        } else if (msg.sender == playerB && encrMove == encPlayerBChoice) {
            movePlayerB = move;
        } else {
            return Moves.None;
        }
        return move;
    }

    function getOutcome() public  returns (Outcomes) {
        Outcomes outcome;

        if (movePlayerA == movePlayerB) {
            outcome = Outcomes.Draw;
        } else if ((movePlayerA == Moves.Rock     && movePlayerB == Moves.Scissor) ||
                   (movePlayerA == Moves.Paper    && movePlayerB == Moves.Rock)     ||
                   (movePlayerA == Moves.Scissor && movePlayerB == Moves.Paper)    ||
                   (movePlayerA != Moves.None     && movePlayerB == Moves.None)) {
            outcome = Outcomes.PlayerA;
        } else {
            outcome = Outcomes.PlayerB;
        }

        reset();  // Reset game before paying to avoid reentrancy attacks

        return outcome;
    }

    // Reset game
    function reset() public {
        playerA = payable(0x0);
        playerB = payable(0x0);
        encPlayerAChoice = 0x0;
        encPlayerBChoice = 0x0;
        movePlayerA = Moves.None;
        movePlayerB = Moves.None;
    }

    // Helper functions
    function getContractBalance() public view returns (uint256) {
        return address(this).balance;
    }

    // Return first character of a given string.
    function getFirstChar(string memory str)
        private
        pure
        returns (uint256 result)
    {
        bytes memory firstByte = bytes(str);
        if (firstByte[0] == 0x31) {
            return 1;
        } else if (firstByte[0] == 0x32) {
            return 2;
        } else if (firstByte[0] == 0x33) {
            return 3;
        } else {
            return 0;
        }
    }
}
