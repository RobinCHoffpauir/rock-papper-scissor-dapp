//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.11;

contract RPSGame {
    bytes32 private constant COMMITMENT_SALT =
        bytes32("0x6a377e3f514827685962");

    struct Player {
        address player;
        bytes32 choice;
        uint256 timestamps;
        // uint256 price;
        bool isJoined;
    }

    // struct Player2 {
    //     address player;
    //     bytes32 choice;
    //     uint timestamps;
    // }

    struct GamePool {
        Player player1;
        Player player2;
        uint256 price;
        uint256 timestamps;
    }

    // enum moves {
    //   keccak256(abi.encodePacked("ROCK")),
    //   keccak256(abi.encodePacked("SCISSORS")),
    //   keccak256(abi.encodePacked("PAPER"))
    // }

    mapping(bytes32 => GamePool) public gamePool;

    //   mapping(address => Player) ;
    bytes32 public ROCK =
        keccak256(abi.encodePacked(msg.sender, "ROCK", block.number)); //keccak256(abi.encodePacked("ROCK"));
    bytes32 public SCISSORS =
        keccak256(abi.encodePacked(msg.sender, "SCISSORS", block.number)); //keccak256(abi.encodePacked("SCISSORS"));
    bytes32 public PAPER =
        keccak256(abi.encodePacked(msg.sender, "PAPER", block.number)); // keccak256(abi.encodePacked("PAPER"));

    function createCommitment(address _player, uint8 _rand)
        private
        pure
        returns (bytes32)
    {
        return keccak256(abi.encodePacked(_player, _rand));
    }

    function getHash() public view returns (bytes32) {
        return keccak256(abi.encodePacked(block.number, block.difficulty));
    }

    function makeMove(bytes2 commitment) public {}

    function joinGame(bytes32 _gamePoolId) public view {
        // gamePool[]
        GamePool memory _gamePool = gamePool[_gamePoolId];
        if (_gamePool.player1.isJoined) {
            _gamePool.player2.player = msg.sender;
            _gamePool.player2.isJoined = true;
            _gamePool.player2.timestamps = block.timestamp;
        } else {
            _gamePool.player1.player = msg.sender;
            _gamePool.player1.isJoined = true;
            _gamePool.player2.timestamps = block.timestamp;
        }
    }

    function fetchAllGame() public view returns(GamePool memory)  {
        return gamePool;
    }

    function getGameDetails(bytes32 _gamePoolId) public view returns(GamePool memory)  {
        return gamePool[_gamePoolId];
    }
    // function joinGame() public {

    // }

    // mappings(address => Player) ;
}
