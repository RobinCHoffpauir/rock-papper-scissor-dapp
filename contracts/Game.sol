//SPDX-License-Identifier: license
pragma solidity ^0.8.11;

contract Game {
    // the variables can also be made constant since they shouldn't change ever!
    // uint8 private constant ROCK = 1;
    // uint8 private constant PAPER = 2;
    // uint8 private constant SCISSORS = 3;
    bytes32 public constant ROCK = keccak256(abi.encodePacked("ROCK"));
    bytes32 public constant SCISSORS = keccak256(abi.encodePacked("SCISSORS"));
    bytes32 public constant PAPER = keccak256(abi.encodePacked("PAPER"));

    mapping(address => bytes32) public choices;

    function play(bytes32 choice) external {
        // require(
        //     choice == ROCK || choice == PAPER || choice == SCISSORS,
        //     "Invalid choice"
        // );
        require(choices[msg.sender] == bytes32(0), "Already made a move");
        choices[msg.sender] = choice;
    }

    function evaluate(address alice, address bob)
        external
        view
        returns (address winner)
    {
        if (choices[alice] == choices[bob]) {
            return address(0);
        }

        if (choices[alice] == ROCK && choices[bob] == PAPER) {
            return bob;
        } else if (choices[alice] == ROCK && choices[bob] == SCISSORS) {
            return alice;
        } else if (choices[alice] == PAPER && choices[bob] == SCISSORS) {
            return bob;
        } else if (choices[alice] == PAPER && choices[bob] == ROCK) {
            return alice;
        } else if (choices[alice] == SCISSORS && choices[bob] == ROCK) {
            return bob;
        } else if (choices[alice] == SCISSORS && choices[bob] == PAPER) {
            return alice;
        }
    }
}
